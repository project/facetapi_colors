<?php
/**
 * @file
 * The facetapi_color_links widget plugin class.
 */

/**
 * Widget that renders facets as a list of clickable colored links.
 */
class FacetapiColorLinks extends FacetapiWidgetLinks {

	private $terms = array();

  /**
   * Recursive function that sets each item's theme hook.
   *
   * The individual items will be rendered by different theme hooks depending on
   * whether or not they are active.
   *
   * @param array &$build
   *   A render array containing the facet items.
   *
   * @return FacetapiWidget
   *   An instance of this class.
   *
   * @see theme_facetapi_link_active()
   * @see theme_facetapi_link_inactive()
   */
  protected function setThemeHooks(array &$build) {
    foreach ($build as $value => &$item) {
      $item['#theme'] = ($item['#active'])
        ? 'facetapi_colorlink_active'
        : 'facetapi_colorlink_inactive';
      if (!empty($item['#item_children'])) {
        $this->setThemeHooks($item['#item_children']);
      }
    }
    return $this;
  }

  /**
   * Overrides FacetapiWidgetLinks::getItemClasses().
   *
   * Sets the base class for colored links facet items.
   */
  public function getItemClasses() {
    return array('facetapi-color-links');
  }

  /**
   * Returns taxonomy term objects by term id list.
   *
   * @param array $tids
   *   A list of taxonomy terms id's.
   *
   * @return array
   *   An array of loaded term objects.
   */
  public function getTerms($tids){
    $this->terms = taxonomy_term_load_multiple($tids);
    return $this->terms;
  }

  /**
   * Returns single taxonomy term object by id.
   *
   * @param $tid
   *   A taxonomy term id.
   *
   * @return object|null
   *   An taxonomy term object if exists, NULL instead
   */
  public function getTermByTid($tid) {
    if (isset($this->terms[$tid])) {
      return $this->terms[$tid];
    }
    return NULL;
  }

  /**
   * Gets list with HEX color values in the specified $field for taxonomy $term.
   *
   * @param $term
   *   A taxonomy term object.
   * @param $field
   *   Field containing the color values for this taxonomy $term.
   *
   * @return array
   *   An array with founded HEX color values.
   */
  public function getColorHexValuesForTerm($term, $field) {
    $term_wrapper = entity_metadata_wrapper('taxonomy_term', $term);
    $colors = array();
    foreach ($term_wrapper->$field as $color) {
      $colors[] = $color->value();
    }
    return $colors;
  }

  /**
   * Transforms the render array for use with theme_item_list().
   *
   * The recursion allows this function to act on the various levels of a
   * hierarchical data set.
   *
   * @param array $build
   *   The items in the facet's render array being transformed.
   *
   * @return array
   *   The "items" parameter for theme_item_list().
   */
  function buildListItems($build) {
    $settings = $this->settings->settings;
    $items = array();

    $this->getTerms(array_keys($build));

    $term_color_field = isset($settings['term_color_field']) ? $settings['term_color_field'] : FALSE;
    if ($term_color_field) {

      // Initializes links attributes, adds rel="nofollow" if configured.
      $attributes = ($settings['nofollow']) ? array('rel' => 'nofollow') : array();
      $attributes += array('class' => $this->getItemClasses());

      // Builds rows.
      foreach ($build as $value => $item) {
        $term = $this->getTermByTid($value);

        if ($term) {
          $colors = $this->getColorHexValuesForTerm($term, $term_color_field);
          // @TODO: make template for this.
          $text = '<span class="facet_colors_box">';
          foreach ($colors as $color) {
            $text .= '<span class="facet_color_box" style="background-color:' . $color . ';" ></span>';
          }
          $text .= '</span><span class="facet_color_name">' . $term->name . '</span>';

          $row = array('class' => array());

          // Allow adding classes via altering.
          if (isset($item['#class'])) {
            $attributes['class'] = array_merge($attributes['class'], $item['#class']);
          }
          // Adding title attribute to color link.
          $attributes['title'] = $term->name;

          // Initializes variables passed to theme hook.
          $variables = array(
            'text' => $text,
            'path' => $item['#path'],
            'count' => $item['#count'],
            'options' => array(
              'attributes' => $attributes,
              'html' => TRUE,
              'query' => $item['#query'],
            ),
          );

          // Adds the facetapi-zero-results class to items that have no results.
          if (!$item['#count']) {
            $variables['options']['attributes']['class'][] = 'facetapi-zero-results';
          }

          // Add an ID to identify this link.
          $variables['options']['attributes']['id'] = drupal_html_id('facetapi-link');

          // If the item has no children, it is a leaf.
          if (empty($item['#item_children'])) {
            $row['class'][] = 'leaf';
          }
          else {
            // If the item is active or the "show_expanded" setting is selected,
            // show this item as expanded so we see its children.
            if ($item['#active'] || !empty($settings['show_expanded'])) {
              $row['class'][] = 'expanded';
              $row['children'] = $this->buildListItems($item['#item_children']);
            }
            else {
              $row['class'][] = 'collapsed';
            }
          }

          // Gets theme hook, adds last minute classes.
          $class = ($item['#active']) ? 'facetapi-active' : 'facetapi-inactive';
          $variables['options']['attributes']['class'][] = $class;

          // Themes the link, adds row to items.
          $row['data'] = theme($item['#theme'], $variables);
          $items[] = $row;
        }
      }

    }
    return $items;
  }

  /**
   * Allows the widget to provide additional settings to the form.
   */
  function settingsForm(&$form, &$form_state) {
    parent::settingsForm($form, $form_state);
    $settings = $this->settings->settings;
    $form['widget']['widget_settings']['collapsible_links'][$this->id] = array(
      'term_color_field' => array(
        '#type' => 'textfield',
        '#title' => t('Term color field machine name'),
        '#default_value' => isset($settings['term_color_field']) ? $settings['term_color_field'] : '',
        '#description' => t('The name of the term field to look for a color hex.'),
        '#states' => array(
          'visible' => array(
            'select[name="widget"]' => array('value' => $this->id),
          ),
        ),
      )
    );
  }
}
