---SUMMARY---

This module creates an Search API color facet block. It allows a user to choose certain colors as options and store
the hex value of the color along with the color name to be shown on the search page.
So for example if you are selling an item with multiple color options you could use this module to display
the color choices in boxes on the product page.

Facet API color works with up to 2 colors products.

---REQUIREMENTS---

This module requires the following modules:

 * Facet API (https://drupal.org/project/facetapi)
 * Entity API (https://drupal.org/project/entity)

---INSTALLATION---

Install as usual.

Place the entirety of this directory in the /modules folder of your Drupal
installation. Navigate to Administer > Extend. Check the 'Enabled' box next
to the 'Facet API Colors' and click the 'Save Configuration' button at the bottom.

For help regarding installation, visit:
https://www.drupal.org/documentation/install/modules-themes/modules-7

---CONFIGURATION---

 * Create Taxonomy vocabulary Color.
 * Add text field Color. Field values must be in Hex format starts with #, for example #ffffff, #ff0000.
 * Go to Configure facet display page and set Display widget to choose Color Links.
 * Set your color field in "Term color field machine name" setting.
 * Then use Color facet as usual Search API facet block.

---CONTACT---

Current Maintainers:
*Johann Janzen (jakoloborodun) - https://www.drupal.org/u/jakoloborodun
